package trimdevelopmentcom.sos;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.List;


public class WearService extends WearableListenerService {

    public static String SERVICE_CALLED_WEAR = "WearActivity";


    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);

        String event = messageEvent.getPath();

        Log.d("Listclicked", event);

        String[] message = event.split("--");

        if (message[0].equals(SERVICE_CALLED_WEAR)) {

            Intent startIntent = new Intent(getBaseContext(), Splash.class);
            startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startIntent.putExtra("pass", true);
            startActivity(startIntent);

        }
    }
}