package trimdevelopmentcom.sos;

/**
 * Created by Macbook on 4/11/16.
 */
public class Object_ner implements Comparable<Object_ner>{

    public String getPlace_name() {
        return place_name;
    }

    public void setPlace_name(String place_name) {
        this.place_name = place_name;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    String place_name;
    String vicinity;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    String icon;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    double distance
            ;
    double lat,lon;

    @Override
    public int compareTo(Object_ner another) {

        return this.getPlace_name().compareTo(another.getPlace_name());
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((place_name == null) ? 0 : place_name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Object_ner other = (Object_ner) obj;
        if (place_name == null) {
            if (other.place_name != null)
                return false;
        } else if (!place_name.equals(other.place_name))
            return false;
        return true;
    }
}
