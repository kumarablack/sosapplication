package trimdevelopmentcom.sos;

import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static trimdevelopmentcom.sos.SetingActivity.getStringFromRetrofitResponse;

/**
 * Created by Macbook on 2/3/17.
 */

public class HelloService extends Service {

    private static final String TAG = "HelloService";
    String jsonmenues_stored;
    File myInternalFile;
    File directory;
    private String filepath = "hasdata";
    private boolean isRunning  = false;

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i(TAG, "Service onStartCommand");

        //Creating new thread for my service
        //Always write your long running tasks in a separate thread, to avoid ANR

        getBooks();
        get_fastaid();
        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent arg0) {
        Log.i(TAG, "Service onBind");
        return null;
    }

    @Override
    public void onDestroy() {

        isRunning = false;

        Log.i(TAG, "Service onDestroy");
    }

    private void get_fastaid(){

        //While the app fetched data we are displaying a progress dialog
        String ROOT_URL ="http://projects.yogeemedia.com/preview/embassy";

        //Creating a rest adapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL)
                .build();

        //Creating an object of our api interface
        Interface api = adapter.create(Interface.class);

        //Defining the method
        api.getfasttaid( new Callback<Response>() {
            @Override
            public void success(Response detailsResponse, Response response2) {

                String detailsString = getStringFromRetrofitResponse(detailsResponse);

                try {
                    JSONObject object = new JSONObject(detailsString);

                    Log.e("List", String.valueOf(object.getJSONArray("contacts")));

                    writeToFile_fest(object);

                    //In here you can check if the "details" key returns a JSONArray or a String

                } catch (JSONException e) {

                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("List",error.toString());

            }});}

    private void writeToFile_fest(JSONObject list) {

        ContextWrapper contextWrapper = new ContextWrapper(this);
        directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);
        myInternalFile = new File(directory, "firstaid");

        if (jsonmenues_stored == null) {
            try {

                FileOutputStream fos = new FileOutputStream(myInternalFile);
                fos.write(list.toString().getBytes());
                fos.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    private void getBooks(){


        String ROOT_URL ="http://projects.yogeemedia.com/preview/embassy";
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL)
                .build();
        Interface api = adapter.create(Interface.class);
        api.getBooks( new Callback<Response>() {
            @Override
            public void success(Response detailsResponse, Response response2) {

                String detailsString = getStringFromRetrofitResponse(detailsResponse);

                try {
                    JSONObject object = new JSONObject(detailsString);

                    Log.e("List2", String.valueOf(object.getJSONArray("contacts")));

                    writeToFile(object);

                    //In here you can check if the "details" key returns a JSONArray or a String

                } catch (JSONException e) {


                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Listd",error.toString());


            }});}

    private void writeToFile(JSONObject list) {

        ContextWrapper contextWrapper = new ContextWrapper(this);
        directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);
        myInternalFile = new File(directory, "embecy");

        Log.e("contacts",list.toString());
        if (jsonmenues_stored == null) {
            try {
                FileOutputStream fos = new FileOutputStream(myInternalFile);
                fos.write(list.toString().getBytes());
                fos.close();

                if(Data_coltrola.Embece_data.size()==0){


                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}