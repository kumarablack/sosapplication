package trimdevelopmentcom.sos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.maps.android.SphericalUtil;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Data_list extends AppCompatActivity {
    private LayoutInflater inflater;
    ListView listView;
    CustomListAdapter_Find find_user;
    String latitude, longitude;
    String next_page_token;
    Button button2;
    Context context = this;
    double dist = 0.0;
    int radius = 5000;
    GlobalClass globalvariable;

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_list);

        globalvariable = (GlobalClass) getApplicationContext();
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listView = (ListView) findViewById(R.id.listView);
        button2 = (Button) findViewById(R.id.button2);
        findViewById(R.id.avloadingIndicatorView).setVisibility(View.VISIBLE);

        Intent intent = getIntent();
        Data_coltrola.places_data.clear();
        Data_coltrola.places_data_final.clear();
        Data_coltrola.al.clear();
        latitude = intent.getStringExtra("latitude");
        longitude = intent.getStringExtra("longitude");
        final String type = intent.getStringExtra("type");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView setting = (TextView) toolbar.findViewById(R.id.setting);
        setSupportActionBar(toolbar);
        TextView back =(TextView)toolbar.findViewById(R.id.info);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Count_down = new Intent(Data_list.this, SetingActivity.class);
                startActivity(Count_down);

            }
        });

        TextView toobar_title = (TextView) toolbar.findViewById(R.id.name_toll2);
        if (type.equalsIgnoreCase("police")) {
            toobar_title.setText("Near By Police Stations ");

        } else if (type.equalsIgnoreCase("hospital")) {
            toobar_title.setText("Near By Hospitals");

        } else if (type.equalsIgnoreCase("bank")) {
            toobar_title.setText("Near By Banks");

        } else if (type.equalsIgnoreCase("pharmacy"))
            toobar_title.setText("Near By Pharmacies");
        near_place(type, longitude, latitude);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                globalvariable.getNext_page_token();
                near_place_more(type, longitude, latitude, radius);
                radius = radius + 3000;
            }
        });

    }

    private void near_place(String type, String mLatitude, String mLongitude) {
        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        sb.append("location=" + mLongitude + "," + mLatitude);
        sb.append("&radius=5000");
        sb.append("&types="+ type);
        sb.append("&sensor=true");
        sb.append("&key=AIzaSyDnUVoF1XOP0QCAZ-NYKdytsfsyNh0TwuI");
        PlacesTask placesTask = new PlacesTask();
        placesTask.execute(sb.toString());
    }

    private void near_place_more(String type, String mLatitude, String mLongitude , int next_page_token) {
        findViewById(R.id.avloadingIndicatorView).setVisibility(View.VISIBLE);

        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        sb.append("location=" + mLongitude + "," + mLatitude);
        sb.append("&types="+type);
        sb.append("&sensor=true");
        sb.append("&radius="+next_page_token);
        sb.append("&key=AIzaSyDnUVoF1XOP0QCAZ-NYKdytsfsyNh0TwuI");
        PlacesTask placesTask = new PlacesTask();
        placesTask.execute(sb.toString());

    }


    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }

        return data;
    }

    /**
     * A class, to download Google Places
     */
    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        @Override
        protected String doInBackground(String... url) {
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }

    }


    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        // Invoked by execute() method of this object
        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                /** Getting the parsed data as a List construct */
                places = placeJsonParser.parse(jObject,context);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            System.out.println("Dahanushka4");

            return places;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(List<HashMap<String, String>> list) {
            for (int i = 0; i < list.size(); i++) {
                MarkerOptions markerOptions = new MarkerOptions();
                HashMap<String, String> hmPlace = list.get(i);

                double lat = Double.parseDouble(hmPlace.get("lat"));
                double lng = Double.parseDouble(hmPlace.get("lng"));
                String name = hmPlace.get("place_name");
                String icon = hmPlace.get("icon");
                String vicinity = hmPlace.get("vicinity");
                String refrence = hmPlace.get("refrence");

                Object_ner data2 = new Object_ner();
                data2.setLat(lat);
                data2.setPlace_name(name);
                data2.setLon(lng);
                data2.setIcon(icon);
                data2.setDistance(00);
                data2.setVicinity(vicinity);

                getDirection(latitude, longitude, lat, lng, name, icon, vicinity,list,data2);
            }

        }


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }


    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask_Two extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance_km = null, trip_time2 = null;


            LatLng position = null;
            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    distance_km = point.get("distance_km");
                    trip_time2 = point.get("time");
                    position = new LatLng(lat, lng);

                    points.add(position);
                }
                System.out.println("trip_time" + trip_time2 + " " + " distance_km" + distance_km);

            }

            // map.addMarker(markerOptions);

        }
    }


    /**
     * A method to download json data from url
     */
    private String downloadUrl_Two(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {

        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    public void getDirection(final String latitude, final String longitude, final double lat, final double lng, final String name, final String icon, final String vicinity, final List<HashMap<String, String>> list2, final Object_ner data2){
        String url = makeURL(Double.valueOf(latitude), Double.valueOf(longitude) , Double.valueOf(lat) , Double.valueOf(lng) );

        final StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        LatLng from = new LatLng(Double.valueOf(latitude),Double.valueOf(longitude));
                        LatLng to = new LatLng(Double.valueOf(lat),Double.valueOf(lng));
                        Double distance = SphericalUtil.computeDistanceBetween(from, to);

                        try {

                            final JSONObject json = new JSONObject(response);
                            JSONArray routeArray = json.getJSONArray("routes");
                            JSONObject routes = routeArray.getJSONObject(0);
                            JSONArray legs = routes.getJSONArray("legs");
                            JSONObject steps = legs.getJSONObject(0);
                            JSONObject distance2 = steps.getJSONObject("distance");
                            double dist2 = Double.parseDouble(distance2.getString("text").replaceAll("[^\\.0123456789]","") );

                            Object_ner data = new Object_ner();
                            data.setLat(lat);
                            data.setPlace_name(name);
                            data.setLon(lng);
                            data.setIcon(icon);
                            data.setDistance(dist2);
                            data.setVicinity(vicinity);
                            Data_coltrola.places_data.add(data);
                            Data_coltrola.places_data_final.add(data2);

                            System.out.println("Data_coltrola.places_data_final.size()"+Data_coltrola.places_data_final.size());
                            System.out.println("Data_coltrola.places_data_final.size()"+Data_coltrola.places_data.size());


                            if (Data_coltrola.places_data_final.size() == Data_coltrola.places_data.size()) {
                                findViewById(R.id.avloadingIndicatorView).setVisibility(View.GONE);

                                Set<Object_ner> hs = new HashSet<Object_ner>();
                                hs.addAll(Data_coltrola.places_data);
                                Data_coltrola.al.clear();
                                Data_coltrola.al.addAll(hs);

                            Comparator<Object_ner> lowtohigh = new Comparator<Object_ner>() {
                                @Override
                                public int compare(Object_ner p1, Object_ner p2) {

                                    return ((int) p1.getDistance() - (int) p2.getDistance());
                                }
                            };

                            Collections.sort(Data_coltrola.al, lowtohigh);

                                find_user = new CustomListAdapter_Find(Data_list.this, Data_coltrola.al);
                                listView.setAdapter(find_user);
                                find_user.notifyDataSetChanged();
                            }

                        }
                        catch (JSONException e) {

                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //Adding the request to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    public class CustomListAdapter_Find extends BaseAdapter {
        private Activity activity;
        private List<Object_ner> movieItems_pro;


        public CustomListAdapter_Find(Activity activity, ArrayList<Object_ner> movieItems_pro) {
            this.activity = activity;
            this.movieItems_pro = movieItems_pro;
        }


        @Override
        public int getCount() {
            return this.movieItems_pro.size();
        }

        @Override
        public Object getItem(int location) {
            return movieItems_pro.get(location);

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            find_user.notifyDataSetChanged();
            if (convertView == null)
                convertView = inflater.inflate(R.layout.places_sell, null);
            find_user.notifyDataSetChanged();
            TableRow tach = (TableRow) convertView.findViewById(R.id.input_layout_phone);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView addres = (TextView) convertView.findViewById(R.id.addres);
            ImageView imag = (ImageView) convertView.findViewById(R.id.icon);

            final Object_ner m = movieItems_pro.get(position);
            name.setText(m.getPlace_name());
            addres.setText(m.getVicinity() + "  " + m.getDistance() + "Km");
            Picasso.with(Data_list.this).load(Data_coltrola.places_data.get(position).getIcon()).into(imag);

            tach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr=" + latitude + "," + longitude + "&daddr=" + "" + m.getLat() + "," + m.getLon()));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);

                }
            });


            //System.out.println(order_number);
            return convertView;
        }

    }

    public String makeURL (double sourcelat, double sourcelog, double destlat, double destlog ){
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString
                .append(Double.toString( sourcelog));
        urlString.append("&destination=");// to
        urlString
                .append(Double.toString( destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=&alternatives=true");
        return urlString.toString();
    }

}

