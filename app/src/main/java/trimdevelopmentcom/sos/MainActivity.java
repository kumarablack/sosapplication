package trimdevelopmentcom.sos;

import android.*;
import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import trimdevelopmentcom.sos.Event_Bus.BusProvider;
import trimdevelopmentcom.sos.Event_Bus.ErrorEvent;
import trimdevelopmentcom.sos.Event_Bus.ServerEvent;
import trimdevelopmentcom.sos.Sever_task.Communicator;

public class MainActivity extends Fragment implements View.OnClickListener {
     Camera.Parameters p;
    private Boolean isFabOpen = false;
    private boolean isLighOn = false;
    private boolean isLighOn_sos = false;
    Location location;
    String SOS_E_mail,name, SOS_E_mail2, sos_phone, email, sos_Phone2, Sos_countdown_masseg, pin, adres, sos_phone_coud, sos_phone_coud2, longitude, latitude;
    int play = 1;
    private View rootView;
    public static Bus bus;
    private Camera camera;
    SmsManager smsManager;
    //double longitude = -122.084095
    PackageManager pm;
    String jsonmenues_stored;
    File myInternalFile;
    File directory;
    int blink =0;
    Thread t;

    ProgressBar mprogressBar;
    ObjectAnimator anim;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private static final int REQUEST_EXTERNAL_STORAGE_RESULT = 1;
    private static final int REQUEST_EXTERNAL_STORAGE_RESULT_SMS = 2;

    private static final int REQUEST_EXTERNAL_STORAGE_RESULT_CALL = 3;

    private static final String jsonFilePath = "F:\\nikos7\\Desktop\\filesForExamples\\jsonFile.json";

    TextView toobar_title;

    private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;
    public  String ambulens, police_number, fire, country_defolt,country_defolt2;
    public MainActivity() {

    }
    @Override
    public void onStop() {
        super.onStop();

        if (camera != null) {
            camera.release();
        }
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://trimdevelopmentcom.sos/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);

        client.disconnect();

    }

    private ImageView fab, fab1, fab2;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    TextView elam, compos, lhit, Ambulance, police, fir;

    MediaPlayer mPlayer;
    public static boolean isSignedIn = false;
    GlobalClass globalvariable;
    DatabaseHandler db;


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    int click_count = 0;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_main, container, false);
        globalvariable = (GlobalClass) getActivity().getApplicationContext();




        longitude=globalvariable.getLongitude();
        latitude= globalvariable.getLatitude();
        adres=globalvariable.getAddres();



        elam = (TextView)rootView.findViewById(R.id.elam);
        compos = (TextView)rootView. findViewById(R.id.compos);
        lhit = (TextView)rootView. findViewById(R.id.lhit);
        Ambulance = (TextView)rootView. findViewById(R.id.Ambulance);
        police = (TextView)rootView. findViewById(R.id.police);
        fir = (TextView) rootView.findViewById(R.id.fir);

        mprogressBar = (ProgressBar) rootView.findViewById(R.id.textView8);
        Drawable img = getActivity().getApplication().getResources().getDrawable(R.drawable.sos_old);
        mprogressBar.setBackground(img);

        anim = ObjectAnimator.ofInt(mprogressBar, "progress", 0, 100);
        anim.setDuration(2000);
        anim.setInterpolator(new DecelerateInterpolator());

        mprogressBar.setOnTouchListener(new View.OnTouchListener() {

          @Override
         public boolean onTouch(View view, MotionEvent motionEvent) {

           if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
               anim.start();

               } else if (motionEvent.getAction() == MotionEvent.ACTION_UP){

                anim.cancel();
                mprogressBar.setProgress(0);
              }
               return true;
                  }

        }
        );


        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {


                if(mprogressBar.getProgress() == 100){

                    Drawable img = getActivity().getApplication().getResources().getDrawable(R.drawable.sos_click);
                    mprogressBar.setBackground(img);
                    send_messeg();
                    Check_plsh();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });



        TextView count = (TextView) rootView.findViewById(R.id.count);

        Ambulance.setOnClickListener(this);
        police.setOnClickListener(this);
        fir.setOnClickListener(this);

        count.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Check_plsh();
                Intent intent = new Intent(getActivity(), Count_down.class);
                startActivity(intent);

            }
        });

        fab1 = (ImageView) rootView.findViewById(R.id.fab1);
        fab2 = (ImageView) rootView.findViewById(R.id.fab2);

        fab_open = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_backward);
        lhit.setOnClickListener(this);
        fab1.setOnClickListener(this);
        fab2.setOnClickListener(this);


        compos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Check_plsh();
                Intent Count_down = new Intent(getActivity(), Compose.class);

                startActivity(Count_down);

            }
        });

        elam.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Check_plsh();

                if (play == 1) {
                    Uri myUri1 = Uri.parse("android.resource://trimdevelopmentcom.sos/" + R.raw.videoplayback);

                    mPlayer = new MediaPlayer();

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {

                    }

                    try {
                        Class<?> cMediaTimeProvider = Class.forName("android.media.MediaTimeProvider");
                        Class<?> cSubtitleController = Class.forName("android.media.SubtitleController");
                        Class<?> iSubtitleControllerAnchor = Class.forName("android.media.SubtitleController$Anchor");
                        Class<?> iSubtitleControllerListener = Class.forName("android.media.SubtitleController$Listener");

                        Constructor constructor = cSubtitleController.getConstructor(Context.class, cMediaTimeProvider, iSubtitleControllerListener);

                        Object subtitleInstance = constructor.newInstance(MainActivity.this, null, null);

                        Field f = cSubtitleController.getDeclaredField("mHandler");

                        f.setAccessible(true);
                        try {
                            f.set(subtitleInstance, new Handler());
                        } catch (IllegalAccessException e) {
                        } finally {
                            f.setAccessible(false);
                        }

                        Method setsubtitleanchor = mPlayer.getClass().getMethod("setSubtitleAnchor", cSubtitleController, iSubtitleControllerAnchor);

                        setsubtitleanchor.invoke(mPlayer, subtitleInstance, null);

                    } catch (Exception e) {
                    }

                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    try {
                        mPlayer.setDataSource(getActivity(), myUri1);
                    } catch (IllegalArgumentException e) {
                    } catch (SecurityException e) {
                    } catch (IllegalStateException e) {
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        mPlayer.prepare();
                    } catch (IllegalStateException e) {
                    } catch (IOException e) {
                    }

                    mPlayer.start();
                    play = 2;
                } else {
                    play = 1;
                    mPlayer.stop();
                }
                // mucick player

            }
        });

        client = new GoogleApiClient.Builder(getActivity()).addApi(AppIndex.API).build();
        return rootView;
    }

    public void send_messeg() {
        DatabaseHandler  db2 = new DatabaseHandler(getActivity());
        Cursor c = db2.getAllContacts();

        System.out.println("all data" + c.getCount());
        if (c.moveToFirst()) {
            name=(c.getString(1));
            email=(c.getString(2));
            sos_phone = c.getString(4);
            sos_phone_coud = c.getString(7);
            sos_phone_coud2 = c.getString(5);
            SOS_E_mail = c.getString(9);
            SOS_E_mail2= c.getString(10);
            sos_Phone2 = c.getString(16);
            Sos_countdown_masseg = c.getString(11);
            pin = c.getString(13);
            country_defolt = c.getString(14);

//            sos_phone.getText().toString(),
//                    Phone.getText().toString(),
//                    sos_Phone2.getText().toString())

        }

        db2.close();

        open_SMS();
    }


    private void openCamera() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                callCameraApp();
            } else {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)) {
                }
                requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                        REQUEST_EXTERNAL_STORAGE_RESULT);
            }
            System.out.println("permission.CAMERA");
        } else {
            callCameraApp();

            System.out.println(" callCameraApp();");
        }


    }




    private void open_SMS() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED) {
                Sms_permition();
            } else {

                if (shouldShowRequestPermissionRationale(android.Manifest.permission.SEND_SMS)) {
                }
                requestPermissions(new String[]{android.Manifest.permission.SEND_SMS},
                        REQUEST_EXTERNAL_STORAGE_RESULT_SMS);
            }
            System.out.println("permission.CAMERA");
        } else {
            Sms_permition();

            System.out.println(" callCameraApp();");
        }


    }

    private void open_CALL(String Number) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                call_now(Number);
            } else {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
                }
                requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE},
                        REQUEST_EXTERNAL_STORAGE_RESULT_CALL);
            }
        } else {
            call_now(Number);

            System.out.println(" callCameraApp();");
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_EXTERNAL_STORAGE_RESULT) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callCameraApp();
            } else {
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        if (requestCode == REQUEST_EXTERNAL_STORAGE_RESULT_SMS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Sms_permition();
            } else {
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        if (requestCode == REQUEST_EXTERNAL_STORAGE_RESULT_CALL) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //call_now(Number);
            } else {
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.lhit:

                animateFAB();
                break;
            case R.id.fab1:

                if (isLighOn) {


                    turnOffFlash();
                    break;
                } else {

                    turnOnFlash();
                    break;
                }


            case R.id.fab2:

                if(blink==0){
                blink(50,1000);
                    blink=1;
                }else{

                    blink=0;
                }
//                camera.stopPreview();


                break;
            case R.id.police:
                //mssege
                Check_plsh();

//                call_now();
                if (globalvariable.getPolice() != null && !globalvariable.getPolice().equalsIgnoreCase(" ")){
                    open_CALL( globalvariable.getPolice());
                    Log.e("globalvariable.getPolice()",globalvariable.getPolice());
                }

                break;
            case R.id.Ambulance:
                Check_plsh();
//                call_now(ambulens);
                if (globalvariable.getAmbulance() != null && !globalvariable.getAmbulance().equalsIgnoreCase(" ")){
                    open_CALL(globalvariable.getAmbulance());
                }
                break;
            case R.id.fir:
                Check_plsh();
//                call_now(fire);
                if (globalvariable.getFir() != null && !globalvariable.getFir().equalsIgnoreCase(" ")){
                    open_CALL(globalvariable.getFir());
                }

                break;

        }
    }

    private void Check_plsh() {
        if (isFabOpen) {
            // lhit.startAnimation(rotate_backward);
            camera.stopPreview();
            camera.release();
            camera = null;
            Drawable img2 = getActivity().getApplication().getResources().getDrawable(R.drawable.lhite);
            lhit.setCompoundDrawablesWithIntrinsicBounds(null, img2, null, null);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "close");

            if (camera != null) {
                camera.release();
            }
        }
    }


    private void call_now(final String number) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.custem_callbo);
        dialog.setTitle("Call Now");
        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.text);


        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + number));
                try {

                    startActivity(callIntent);
                } catch (ActivityNotFoundException ex) {
                }
                dialog.dismiss();

            }
        });

        dialog.show();

    }

    public void animateFAB() {
        if (isFabOpen) {

            Drawable img = getActivity().getApplication().getResources().getDrawable(R.drawable.lhite);
            lhit.setCompoundDrawablesWithIntrinsicBounds(null, img, null, null);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            turnOffFlash();
            isFabOpen = false;
            Log.d("Raj", "close");


        } else {
            openCamera();
            Drawable img = getActivity().getApplication().getResources().getDrawable(R.drawable.lhite_click);
            lhit.setCompoundDrawablesWithIntrinsicBounds(null, img, null, null);
            // lhit.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;

        }
    }


    @Override
    public void onStart() {
        super.onStart();


    }



    public void Sms_permition() {

        String map_link="http://realtimesos.emergencyhelpapp.com/sos_user/sos_map.php?lat="+latitude+"&lon="+longitude+"&name=test";
        String final_mseg = Sos_countdown_masseg + "  " + "Location" + adres + "  " + "Map Link" +" "+ map_link;
        String final_number =   sos_phone;


        if (sos_phone_coud2 != null && !sos_phone_coud2.equalsIgnoreCase(" ")) {

            String final_number2 =  sos_Phone2;
            System.out.println("final_number2" + final_number2);


            sendSMS(final_number2, final_mseg, final_number, adres);
            send_email();

        } else {
            System.out.println("final_number" + final_number);

            sendSMS("0", final_mseg, final_number, adres);
            send_email();

        }
    }

    public void send_email() {

        LatLng location = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
        String map_link="http://realtimesos.emergencyhelpapp.com/sos_user/sos_map.php?lat="+latitude+"&lon="+longitude+"&name=test";

//        String map_link = "https://www.google.lk/maps/@" + latitude + "," + longitude + ",16z?hl=en";
        String final_mseg = Sos_countdown_masseg + "  " ;
        String addres = adres;

        String Name = name;
        String to_email = SOS_E_mail;
        String to_email2 = SOS_E_mail2;
        String for_email = email;


        if (to_email2 != null && !to_email2.equalsIgnoreCase(" ")) {
             String final_mseg2=Sos_countdown_masseg+"  "+"Location"+adres;

            usePost(to_email2, for_email, final_mseg,Name,addres,map_link);
            usePost(to_email, for_email, final_mseg,Name,addres,map_link);
//            usePost(to_email, for_email, final_mseg);
            System.out.println("Location"+for_email);
            System.out.println("Location"+to_email2);

        } else {
            usePost(to_email, for_email, final_mseg,Name,addres,map_link);
        }


    }


    public void sendSMS(String phoneNo, String msg, String phoneNo2, String Addres) {

        LatLng location2 = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
        String location = Double.valueOf(latitude) + "," + Double.valueOf(longitude);

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        System.out.println(dateFormat.format(cal.getTime())); //2014/08/06 16:00:22
        PendingIntent sentIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent("SMS_SENT"), 0);
        PendingIntent deliveredIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent("SMS_DELIVERED"), 0);

        try {
            smsManager = SmsManager.getDefault();
            if (!phoneNo.equalsIgnoreCase("0")) {

                smsManager.sendTextMessage(phoneNo2, null, msg, sentIntent, deliveredIntent);
                smsManager.sendTextMessage(phoneNo, null, msg, sentIntent, deliveredIntent);
                System.out.println("phoneNo"+phoneNo2);
                globalvariable.setPush_addres_gold(Addres);
                globalvariable.setPush_location_gold(String.valueOf(location));
                globalvariable.setPush_messeg_one_gold(phoneNo);
                globalvariable.setPush_messeg_two_gold(phoneNo2);
                globalvariable.setMsg_stetes("Was Sent");
                globalvariable.setPush_date_gold(dateFormat.format(cal.getTime()));
                send_susses();

            } else {

                smsManager.sendTextMessage(phoneNo2, null, msg, sentIntent, deliveredIntent);

                System.out.println("phoneNo" + phoneNo2);


                globalvariable.setPush_addres_gold(Addres);
                globalvariable.setPush_location_gold(String.valueOf(location));
                globalvariable.setPush_messeg_one_gold(phoneNo);
                globalvariable.setPush_messeg_two_gold("0");
                globalvariable.setMsg_stetes("Was Sent");
                globalvariable.setPush_date_gold(dateFormat.format(cal.getTime()));
                send_susses();


            }


            // save messeg in android
        } catch (Exception ex) {
//            Toast.makeText(getApplicationContext(),ex.getMessage().toString(),
//                    Toast.LENGTH_LONG).show();
//            ex.printStackTrace();printStackTrace

            if (!phoneNo2.equalsIgnoreCase("0")) {

                smsManager.sendTextMessage(phoneNo2, null, msg, sentIntent, deliveredIntent);

                System.out.println("phoneNo_aafa" + phoneNo);
                smsManager.sendTextMessage(phoneNo, null, msg, sentIntent, deliveredIntent);


                globalvariable.setPush_addres_gold(Addres);
                globalvariable.setPush_location_gold(String.valueOf(location));
                globalvariable.setPush_messeg_one_gold(phoneNo);
                globalvariable.setPush_messeg_two_gold(phoneNo2);
                globalvariable.setMsg_stetes("Wasn't Sent");
                globalvariable.setPush_date_gold(dateFormat.format(cal.getTime()));
                send_susses();

            } else {

                smsManager.sendTextMessage(phoneNo, null, msg, sentIntent, deliveredIntent);
                // send_susses(phoneNo,msg);

                System.out.println("phoneNo" + phoneNo);


                globalvariable.setPush_addres_gold(Addres);
                globalvariable.setPush_location_gold(String.valueOf(location));
                globalvariable.setPush_messeg_one_gold(phoneNo);
                globalvariable.setPush_messeg_two_gold("0");
                globalvariable.setMsg_stetes("Wasn't Sent");
                globalvariable.setPush_date_gold(dateFormat.format(cal.getTime()));
                send_susses();


            }

        }
    }

    private void send_susses() {

        Obj_Messag item = new Obj_Messag();
        item.setPush_messeg_two_gold(globalvariable.getPush_messeg_two_gold());
        item.setPush_messeg_one_gold(globalvariable.getPush_messeg_one_gold());
        item.setPush_email_one_gold("Dhanushka@gmail.com");
        item.setPush_email_two_gold("Dhanushka@gmail.com");
        item.setPush_location_gold(globalvariable.getPush_location_gold());
        item.setPush_addres_gold(globalvariable.getPush_addres_gold());
        item.setPush_date_gold(globalvariable.getPush_date_gold());
        item.setMsg_stetes("Send!");
        DatabaseHandler dbh = new DatabaseHandler(getActivity());
        dbh.insertContact_messeg(item);
    }


    @Override
    public void onResume() {
        super.onResume();


//        checkPlayServices();
        BusProvider.getInstance().register(this);


    }

    private void usePost(String to_email, String for_email, String messeg,String Name,String addres,String map_link){
        Communicator  communicator = new  Communicator();
        communicator.Email_Post(to_email,for_email,messeg,Name,addres,map_link);

    }



    @Override
    public void onPause(){
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void onServerEvent(ServerEvent serverEvent){
        if(serverEvent.getServerResponse().getMessage() != null){
        }

    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent){
    }
    private void callCameraApp() {


         pm = getActivity().getPackageManager();
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Log.e("err", "Device has no camera!");
            return;
        }

        camera = Camera.open();
        p = camera.getParameters();


}



    private void blink(final int delay, final int times) {
         t = new Thread() {
            public void run() {
                try {

                    for (int i=0; i < times*2; i++) {
                        if(blink==1){
                            if (isLighOn) {
                                turnOffFlash();
                            } else {
                                turnOnFlash();
                            }
                            sleep(delay);

                        }else{
                            turnOffFlash();
                            break;

                        }

                    }

                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        t.start();

    }

    private void turnOnFlash() {
        if (!isLighOn) {
            if (camera == null || p == null) {
                return;
            }

            p = camera.getParameters();
            p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(p);
            camera.startPreview();
            isLighOn = true;
        }

    }

    private void turnOffFlash() {
        if (isLighOn) {
            if (camera == null || p == null) {
                return;
            }
            p = camera.getParameters();
            p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(p);
            camera.stopPreview();
            isLighOn = false;
        }
    }




}