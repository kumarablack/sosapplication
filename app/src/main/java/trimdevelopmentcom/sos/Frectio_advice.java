package trimdevelopmentcom.sos;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static trimdevelopmentcom.sos.SetingActivity.getStringFromRetrofitResponse;


public class Frectio_advice extends Fragment implements View.OnClickListener  {

    ListView list_d;
    private LayoutInflater inflater;
    TextView map,embce,sos,notifi,add;
    String jsonmenues_stored;
    File myInternalFile;
    File directory;
    Spinner spinner_of;
    Resources res;
    private String filepath = "hasdata";
    SweetAlertDialog pDialog;
    CustomListAdapter_helth apter_Rides;
    View rootView;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frectio_advice, container, false);
        ContextWrapper contextWrapper = new ContextWrapper(getActivity());
        directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);
        myInternalFile = new File(directory, "firstaid");

        list_d=(ListView)rootView.findViewById(R.id.list_helth);

        if (!myInternalFile.exists()) {
            pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Loading");
            pDialog.setCancelable(false);


        } else {

            if(Data_coltrola.Embece_helth.size()==0){
                Jso_filereder();

            } else {

                apter_Rides  = new CustomListAdapter_helth(getActivity(), Data_coltrola.Embece_helth);
                list_d.setAdapter(apter_Rides);


            }

        }


        return rootView;
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {


        }

    }

    public class CustomListAdapter_helth extends BaseAdapter {
        private Activity activity;
        private List<Obj_Helth> movieItems_pro;


        public CustomListAdapter_helth(Activity activity, ArrayList<Obj_Helth> movieItems_pro) {
            this.activity = activity;
            this.movieItems_pro = movieItems_pro;
        }

        @Override
        public int getCount() {
            return this.movieItems_pro.size();
        }

        @Override
        public Object getItem(int location) {
            return movieItems_pro.get(location);

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            inflater = (LayoutInflater) getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);



            if (convertView == null)

                convertView = inflater.inflate(R.layout.helth_sell, null);

            TableRow tach = (TableRow) convertView.findViewById(R.id.tableRow2);
            ImageView drop = (ImageView) convertView.findViewById(R.id.imge);
            TextView topic = (TextView) convertView.findViewById(R.id.topic);

            final Obj_Helth m = movieItems_pro.get(position);

tach.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        Intent bank = new Intent(getActivity(), Helth_detilse.class);
        bank.putExtra("message", m.getFirstaid_details().toString());
        startActivity(bank);

    }
});

try {
    String result = m.getFirstaid_image().substring(m.getFirstaid_image().lastIndexOf("/") + 1);

    StringTokenizer tokens = new StringTokenizer(result, ".");
    final String image = tokens.nextToken();//
    String uri = "@drawable/"+image;
    int imageResource = getResources().getIdentifier(uri, null,getActivity().getPackageName());

    Drawable res = getResources().getDrawable(imageResource);
    drop.setImageDrawable(res);
}catch (Exception nsee){

}

            topic.setText(m.getFirstaid_title());
            m.getFirstaid_image();
            return convertView;
        }
    }




    private void Jso_filereder() {
        String filepath = "hasdata";
        Log.e("List","jsonMainNode.toString()");

        ContextWrapper contextWrapper = new ContextWrapper(getActivity());
        directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);
        myInternalFile = new File(directory, "firstaid");

        //read

        if (myInternalFile.exists()) {


            FileInputStream fis = null;
            try {
                fis = new FileInputStream(myInternalFile);
                DataInputStream in = new DataInputStream(fis);
                BufferedReader br =
                        new BufferedReader(new InputStreamReader(in));
                jsonmenues_stored = br.readLine();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            JSONObject jObject = null;
            try {
                jObject = new JSONObject(jsonmenues_stored);
                JSONArray jsonMainNode = jObject.optJSONArray("contacts");
                int lengthJsonArr = jsonMainNode.length();
                Data_coltrola.Embece_helth.clear();

                for (int i = 0; i < lengthJsonArr; i++) {

                    JSONObject menuObject = jsonMainNode.getJSONObject(i);
                    String firstaid_title = menuObject.getString("firstaid_title");
                    String firstaid_image = menuObject.getString("firstaid_image");
                    JSONArray firstaid_details = menuObject.getJSONArray("firstaid_details");

                    Obj_Helth  Embicy = new   Obj_Helth();
                    Embicy.setFirstaid_details(firstaid_details);
                    Embicy.setFirstaid_image(firstaid_image);
                    Embicy.setFirstaid_title(firstaid_title);
                    Data_coltrola.Embece_helth.add(Embicy);

                }
                apter_Rides  = new CustomListAdapter_helth(getActivity(), Data_coltrola.Embece_helth);
                list_d.setAdapter(apter_Rides);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }}
}
