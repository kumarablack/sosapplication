package trimdevelopmentcom.sos;

import android.*;
import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;


import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.squareup.otto.Produce;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import trimdevelopmentcom.sos.Event_Bus.ErrorEvent;
import trimdevelopmentcom.sos.Event_Bus.ServerEvent;
import trimdevelopmentcom.sos.Sever_task.Communicator;
import trimdevelopmentcom.sos.models.Object_contect;
import trimdevelopmentcom.sos.models.Object_notification;
import trimdevelopmentcom.sos.models.Object_register;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class SetingActivity extends RuntimePermissionsActivity  {
    List<String> country = new ArrayList<String>();
    EditText name,E_mail,Phone,sos_phone,sos_Phone2,SOS_E_mail,Sos_masseg,Sos_countdown_masseg,pin,SOS_E_mail_sos2;
    String couflag_call,couflag_2_call,pngName_sos_add_call,cuntry_name;
    ImageView couflag,add_contect;
    ImageView couflag_2;
    ImageView couflag_add;
    ImageView cancel;
    ImageView cancel_email;
    ArrayList<Object_contect> contactList;
    ActionBar actionbar;
    TextView textview;
    Toolbar.LayoutParams layoutparams;
    Cursor cursor;
    int counter;
    private static final int REQUEST_PERMISSIONS = 20;
    Matcher matcher2,matcher;
    CustomListAdapter adapter;
    private Handler updateBarHandler;
    private static  final String TAG = "Communicator";
    private static final String SERVER_URL = "http://realtimesos.emergencyhelpapp.com/sos_user/application_api";
    String jsonmenues_stored;
    File myInternalFile,myInternalFile2;
    File directory;
    SweetAlertDialog pDialog;
     ProgressDialog pDialog_o;
    String sos_phone_final, sos_phone2_final,sos_phone2_final2;
    String token;
    private LayoutInflater inflater;
    TextView add,history,add_email;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    Button Save;
    public static final String REGISTRATION_SUCCESS = "RegistrationSuccess";
    public static final String REGISTRATION_ERROR = "RegistrationError";
     DatabaseHandler db;
    GlobalClass globalvariable;
    AlertDialog alertDialog;

    private String filepath = "hasdata";

    GoogleCloudMessaging gcmObj;
//    Context applicationContext;
    String regId = "";
    Communicator  communicator;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    AsyncTask<Void, Void, String> createRegIdTask;

    public static final String REG_ID = "regId";
    public static final String EMAIL_ID = "eMailId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seting);
        ContextWrapper contextWrapper = new ContextWrapper(getApplication());
        directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);
        myInternalFile = new File(directory, "firstaid");

        ContextWrapper contextWrapper2 = new ContextWrapper(this);
        directory = contextWrapper2.getDir(filepath, Context.MODE_PRIVATE);
        myInternalFile2 = new File(directory, "embecy");

        if (!myInternalFile.exists() || myInternalFile2.exists()) {

            Intent intent = new Intent(this, HelloService.class);
            startService(intent);

        } else {
        }
        ContextWrapper contextWrapper3 = new ContextWrapper(SetingActivity.this);
        directory = contextWrapper3.getDir(filepath, Context.MODE_PRIVATE);
        myInternalFile = new File(directory, "firstaid");
        pDialog_o = new ProgressDialog(this);
        pDialog_o.setMessage("Reading contacts.....");
        pDialog_o.setCancelable(false);
        //read
        contactList = new ArrayList<Object_contect>();

        communicator = new Communicator();

        globalvariable = (GlobalClass) getApplicationContext();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView back =(TextView)toolbar.findViewById(R.id.info);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatabaseHandler db = new DatabaseHandler(SetingActivity.this);
                Cursor c = db.getAllContacts();
                if (c.moveToFirst()) {

                    Intent Count_down = new Intent(SetingActivity.this, Home_page.class);
                    startActivity(Count_down);
                    finish();

                }else{
                }
                db.close();
            }
        });

         pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);


        db = new DatabaseHandler(SetingActivity.this);

        Cursor c2 = db.getAllContacts();
        if (!c2.moveToFirst()) {
        RegisterUser();

        }

        name =(EditText)findViewById(R.id.name);
        final RelativeLayout input_layout_phone_2 =(RelativeLayout)findViewById(R.id.input_layout_phone_2);
        input_layout_phone_2.setVisibility(View.GONE);
        final RelativeLayout input_emsil =(RelativeLayout)findViewById(R.id.input_emsil);


        E_mail =(EditText)findViewById(R.id.E_mail);
        Phone =(EditText)findViewById(R.id.Phone);
        sos_phone =(EditText)findViewById(R.id.sos_phone);
        sos_Phone2 =(EditText)findViewById(R.id.sos_Phone2);
        SOS_E_mail =(EditText)findViewById(R.id.SOS_E_mail);
        SOS_E_mail_sos2 =(EditText)findViewById(R.id.SOS_E_mail_sos2);

        Sos_masseg =(EditText)findViewById(R.id.Sos_masseg);
        add_email =(TextView)findViewById(R.id.add_email);

        Sos_countdown_masseg =(EditText)findViewById(R.id.Sos_countdown_masseg);
        pin =(EditText)findViewById(R.id.pin);
        history =(TextView)findViewById(R.id.history);

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(db.getCount()>0) {

                    Intent emb = new Intent(SetingActivity.this, Histori_messeg.class);
                    finish();
                    startActivity(emb);

                }else{

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            SetingActivity.this);

                    alertDialogBuilder.setTitle("You Haven't history message");
                    alertDialogBuilder
                            .setCancelable(false)
                            .setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, close
                                    // current activity
                                    dialog.cancel();
                                }
                            });
//
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
                }

        });

        SharedPreferences prefs2 = getSharedPreferences("UserDetails",
                Context.MODE_PRIVATE);
        String registrationId = prefs2.getString(REG_ID, "");

        if (!TextUtils.isEmpty(registrationId)) {
//            Intent i = new Intent(SetingActivity.this, GreetingActivity.class);
//            i.putExtra("regId", registrationId);
//            startActivity(i);
//            finish();
        }
//        RegisterUser();


        add_contect =(ImageView)findViewById(R.id.add_contect);

        couflag =(ImageView)findViewById(R.id.couflag);
        couflag_2 =(ImageView)findViewById(R.id.couflag_2);
        couflag_add =(ImageView)findViewById(R.id.couflag_add);
        add =(TextView)findViewById(R.id.add);

        cancel_email=(ImageView)findViewById(R.id.cancel_email);
        cancel=(ImageView)findViewById(R.id. cancel);

        SharedPreferences prefs = getSharedPreferences("currency", MODE_WORLD_READABLE);
       final String couflag_get = prefs.getString("pngName_curent", "gb");
        final String couflag_2_get = prefs.getString("pngName_sos", "gb");
        final String pngName_sos_add_get = prefs.getString("pngName_sos_add", "gb");
     cuntry_name = prefs.getString("cuntry_name", "United States");

       // cuntry_name = prefs.getString("cuntry_name", "United States");
         couflag_call = prefs.getString("tp_cord_curent", "44");
         couflag_2_call = prefs.getString("tp_cord_sos", "44");
         pngName_sos_add_call = prefs.getString("tp_cord_sos_add", "44");


        System.out.println("couflag_get"+couflag_2_call+" "+couflag_call+" "+pngName_sos_add_call);

        couflag.setImageResource(SetingActivity.this.getResources().getIdentifier("drawable/flag_" + couflag_get, null, SetingActivity.this.getPackageName()));
        couflag_2.setImageResource(SetingActivity.this.getResources().getIdentifier("drawable/flag_" + couflag_2_get, null, SetingActivity.this.getPackageName()));
        couflag_add.setImageResource(SetingActivity.this.getResources().getIdentifier("drawable/flag_" + pngName_sos_add_get, null, SetingActivity.this.getPackageName()));

        Save=(Button)findViewById(R.id.Save);
        Cursor c = db.getAllContacts();
        if (c.moveToFirst())
        {
            name.setText(c.getString(1));
            E_mail.setText(c.getString(2));
            Phone.setText(c.getString(15));
            sos_phone.setText(c.getString(14));



            if (c.getString(16) != null && !c.getString(16).equalsIgnoreCase(" ") ) {

                input_layout_phone_2.setVisibility(View.VISIBLE);
                sos_Phone2.setText(c.getString(16));

            } else {

                input_layout_phone_2.setVisibility(View.GONE);
            }

            if (c.getString(10) != null && !c.getString(10).equalsIgnoreCase(" ")  ) {

                input_emsil.setVisibility(View.VISIBLE);
                SOS_E_mail_sos2.setText(c.getString(10));

            } else {

                input_emsil.setVisibility(View.GONE);
            }

            SOS_E_mail.setText(c.getString(9));
            Sos_masseg.setText(c.getString(11));
            Sos_countdown_masseg.setText(c.getString(12));
            pin.setText(c.getString(13));

        }else {
            if (globalvariable.getName() != null && !globalvariable.getName().equalsIgnoreCase(" ")){
                name.setText(globalvariable.getName());

            }
            if (globalvariable.getE_mail() != null && !globalvariable.getE_mail().equalsIgnoreCase(" ")){
                E_mail.setText(globalvariable.getE_mail());
            }
            if ( globalvariable.getPhone() != null && !globalvariable.getPhone().equalsIgnoreCase(" ")){
                Phone.setText(globalvariable.getPhone());


            }if (globalvariable.getSos_phone() != null && !globalvariable.getSos_phone().equalsIgnoreCase(" ")){
                sos_phone.setText(globalvariable.getSos_phone());

            }
            if (globalvariable.getSos_Phone2() != null && !globalvariable.getSos_Phone2().equalsIgnoreCase(" ")){
                add.setVisibility(View.GONE);
                input_layout_phone_2.setVisibility(View.VISIBLE);
                sos_Phone2.setText(globalvariable.getSos_Phone2());
            }
            if ( globalvariable.getSOS_E_mail() != null && !globalvariable.getSOS_E_mail().equalsIgnoreCase(" ")){
                SOS_E_mail.setText(globalvariable.getSOS_E_mail());
            }


            if (globalvariable.getSos_masseg()!= null && !globalvariable.getSos_masseg().equalsIgnoreCase(" ")){
                Sos_masseg.setText(globalvariable.getSos_masseg());

            }
            if (globalvariable.getSos_countdown_masseg() != null && !globalvariable.getSos_countdown_masseg().equalsIgnoreCase(" ")){
                Sos_countdown_masseg.setText(globalvariable.getSos_countdown_masseg());
            }
            if ( globalvariable.getPin() != null && !globalvariable.getPin().equalsIgnoreCase(" ")){
                pin.setText(globalvariable.getPin());
            }
            if ( globalvariable.getSOS_E_mail_sos2() != null && !globalvariable.getSOS_E_mail_sos2().equalsIgnoreCase(" ")){
                SOS_E_mail_sos2.setText(globalvariable.getSOS_E_mail_sos2());
            }
        }

        db.close();

        add_contect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
////                pDialog_o.show();
//                if(contactList.size()>0){
////                    pDialog_o.cancel();
//                    Contect_popup();
//                }else{
//
//                    read_mobilenumber();
//                }
            }
        });



        couflag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                globalvariable.setName(name.getText().toString());
                globalvariable.setE_mail(E_mail.getText().toString());
                globalvariable.setPhone(Phone.getText().toString());
                globalvariable.setSos_phone(sos_phone.getText().toString());
                globalvariable.setSos_Phone2(sos_Phone2.getText().toString());
                globalvariable.setSOS_E_mail(SOS_E_mail.getText().toString());
                globalvariable.setSos_countdown_masseg(Sos_countdown_masseg.getText().toString());
                globalvariable.setSos_masseg(Sos_masseg.getText().toString());
                globalvariable.setPin(pin.getText().toString());
                globalvariable.setCouflag(couflag_get);
                globalvariable.setCouflag_2(couflag_2_get);
                globalvariable.setCouflag_add(pngName_sos_add_get);
                globalvariable.setSOS_E_mail_sos2(SOS_E_mail_sos2.getText().toString());


                Intent emb = new Intent(SetingActivity.this, Select_cuntry.class);
                emb.putExtra("flag", "defolt");
                finish();
                startActivity(emb);

            }
        });
        couflag_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                globalvariable.setName(name.getText().toString());
                globalvariable.setE_mail(E_mail.getText().toString());
                globalvariable.setPhone(Phone.getText().toString());
                globalvariable.setSos_phone( sos_phone.getText().toString());
                globalvariable.setSos_Phone2( sos_Phone2.getText().toString());
                globalvariable.setSOS_E_mail(SOS_E_mail.getText().toString());
                globalvariable.setSos_countdown_masseg(Sos_countdown_masseg.getText().toString());
                globalvariable.setSos_masseg(Sos_masseg.getText().toString());
                globalvariable.setPin(pin.getText().toString());
                globalvariable.setCouflag(couflag_get);
                globalvariable.setCouflag_2(couflag_2_get);
                globalvariable.setCouflag_add(pngName_sos_add_get);
                globalvariable.setSOS_E_mail_sos2(SOS_E_mail_sos2.getText().toString());

                Intent emb = new Intent(SetingActivity.this, Select_cuntry.class);
                emb.putExtra("flag", "sos");
                finish();
                startActivity(emb);

            }
        });
        couflag_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                globalvariable.setName(name.getText().toString());
                globalvariable.setE_mail(E_mail.getText().toString());
                globalvariable.setPhone(Phone.getText().toString());
                globalvariable.setSos_phone( sos_phone.getText().toString());
                globalvariable.setSos_Phone2( sos_Phone2.getText().toString());
                globalvariable.setSOS_E_mail(SOS_E_mail.getText().toString());
                globalvariable.setSos_countdown_masseg(Sos_countdown_masseg.getText().toString());
                globalvariable.setSos_masseg(Sos_masseg.getText().toString());
                globalvariable.setPin(pin.getText().toString());
                globalvariable.setCouflag(couflag_get);
                globalvariable.setCouflag_2(couflag_2_get);
                globalvariable.setCouflag_add(pngName_sos_add_get);
                globalvariable.setSOS_E_mail_sos2(SOS_E_mail_sos2.getText().toString());

                Intent emb = new Intent(SetingActivity.this, Select_cuntry.class);
                emb.putExtra("flag", "sos_add");
                finish();
                startActivity(emb);

            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //add.setVisibility(View.GONE);
                input_layout_phone_2.setVisibility(View.VISIBLE);

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                add.setVisibility(View.VISIBLE);
                sos_Phone2.setText("");
                input_layout_phone_2.setVisibility(View.GONE);

            }
        });


        add_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //add_email.setVisibility(View.GONE);
                input_emsil.setVisibility(View.VISIBLE);


            }
        });

        cancel_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // add.setVisibility(View.VISIBLE);
                SOS_E_mail_sos2.getText().clear();
                SOS_E_mail_sos2.setText("");
                input_emsil.setVisibility(View.GONE);

            }
        });


        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    validetion();

            }
        });

    }

    @Override
    public void onPermissionsGranted(int requestCode) {

    }


    private void read_mobilenumber() {

        updateBarHandler =new Handler();
        // Since reading contacts takes more time, let's run it on a separate thread.
        new Thread(new Runnable() {
            @Override
            public void run() {

                getContacts();
            }
        }).start();
        // Set onclicklistener to the list item.

    }

    private void Update_seting() {


        if (db.updateinsertContact
                (Integer.parseInt("1"),
                        name.getText().toString(),
                        E_mail.getText().toString(),
                        sos_phone_final ,
                        sos_phone2_final,
                        sos_phone2_final2,
                        couflag_call,
                        couflag_2_call,
                        pngName_sos_add_call,
                        SOS_E_mail.getText().toString(),
                        SOS_E_mail_sos2.getText().toString(),
                        Sos_masseg.getText().toString(),
                        Sos_countdown_masseg.getText().toString(),
                        pin.getText().toString(),
                        cuntry_name,
                        sos_phone.getText().toString(),
                        Phone.getText().toString(),
                        sos_Phone2.getText().toString()))

        db.close();
//        usePost( E_mail.getText().toString(),name.getText().toString(),sos_phone.getText().toString(),"tokent");


//        usePost( E_mail.getText().toString(),name.getText().toString(),sos_phone.getText().toString(),"hfs");
        Intent itent = new Intent(SetingActivity.this, Splash.class);
        startActivity(itent);
        finish();


    }

    private void validetion() {

                if(name.getText().toString().isEmpty()){

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SetingActivity.this);
                    alertDialogBuilder.setTitle("SOS Application");
                    alertDialogBuilder
                            .setMessage("Please Enter Your Nmae")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                } else if(E_mail.getText().toString().isEmpty()) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SetingActivity.this);
                    alertDialogBuilder.setTitle("SOS Application");
                    alertDialogBuilder
                            .setMessage("Please Enter Your Email")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                } else if(Phone.getText().toString().isEmpty()) {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SetingActivity.this);
                        alertDialogBuilder.setTitle("SOS Application");
                        alertDialogBuilder
                                .setMessage("Please Enter Your Phone number")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        alertDialog = alertDialogBuilder.create();
                        alertDialog.show();


                } else if(validateUsing_libphonenumber(couflag_call, Phone.getText().toString()) == false){


                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SetingActivity.this);
                        alertDialogBuilder.setTitle("SOS Application");
                        alertDialogBuilder
                                .setMessage("Please Enter Your Phone number")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        alertDialog = alertDialogBuilder.create();
                        alertDialog.show();


                } else if(sos_phone.getText().toString().isEmpty()){

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SetingActivity.this);
                        alertDialogBuilder.setTitle("SOS Application");
                        alertDialogBuilder
                                .setMessage("Please Enter Your Phone number")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();


                }else if(validateUsing_libphonenumber2(couflag_2_call, sos_phone.getText().toString()) == false){

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SetingActivity.this);
                        alertDialogBuilder.setTitle("SOS Application");
                        alertDialogBuilder
                                .setMessage("Please Enter Your Phone number")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                }else if((!sos_Phone2.getText().toString().isEmpty()) && validateUsing_libphonenumber3(pngName_sos_add_call, sos_Phone2.getText().toString()) == false){

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SetingActivity.this);
                    alertDialogBuilder.setTitle("SOS Application");
                    alertDialogBuilder
                            .setMessage("Please Enter Your Phone number")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    alertDialog = alertDialogBuilder.create();
                    alertDialog.show();


                }
                else if(pin.getText().toString().isEmpty()){

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SetingActivity.this);
                    alertDialogBuilder.setTitle("SOS Application");
                    alertDialogBuilder
                            .setMessage("Please Enter Your Pinn")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }else if(SOS_E_mail.getText().toString().isEmpty()){

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SetingActivity.this);
                    alertDialogBuilder.setTitle("SOS Application");
                    alertDialogBuilder
                            .setMessage("Please Enter Your Email")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }else if(Sos_masseg.getText().toString().isEmpty()){

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SetingActivity.this);
                    alertDialogBuilder.setTitle("SOS Application");
                    alertDialogBuilder
                            .setMessage("Please Enter Your Sos Masseg")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }else if(Sos_countdown_masseg.getText().toString().isEmpty()){

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SetingActivity.this);
                    alertDialogBuilder.setTitle("SOS Application");
                    alertDialogBuilder
                            .setMessage("Please Enter Your countdown_masseg")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                }else {

                    Cursor c = db.getAllContacts();
                    if (c.moveToFirst())
                    {
                        Update_seting();
                    }else {
                        insert_db();
                    }

                }
    }

    private void insert_db() {


        db.addinsertContact(name.getText().toString(),
                E_mail.getText().toString(),
                sos_phone_final ,
                sos_phone2_final,
                sos_phone2_final2,
                couflag_call,
                couflag_2_call,
                pngName_sos_add_call,
                SOS_E_mail.getText().toString(),
                SOS_E_mail_sos2.getText().toString(),
                Sos_masseg.getText().toString(),
                Sos_countdown_masseg.getText().toString(),
                pin.getText().toString(),
                 cuntry_name,
                sos_phone.getText().toString(),
                Phone.getText().toString(),
                sos_Phone2.getText().toString());

                 db.close();

        usePost( E_mail.getText().toString(),name.getText().toString(),sos_phone.getText().toString(),token);


    }

    // When Register Me button is clicked
    public void RegisterUser() {
        {
            //Initializing our broadcast receiver
            mRegistrationBroadcastReceiver = new BroadcastReceiver() {

                //When the broadcast received
                //We are sending the broadcast from GCMRegistrationIntentService

                @Override
                public void onReceive(Context context, Intent intent) {
                    //If the broadcast has received with success
                    //that means device is registered successfully
                    if(intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)){
                        //Getting the registration token from the intent
                         token = intent.getStringExtra("token");
                        //Displaying the token as toast

//sout
                        System.out.println( "Registration token:" + token);
                        //if the intent is not with success then displaying error messages
                    } else if(intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)){
                    } else {
                    }
                }
            };

            //Checking play service is available or not
            int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

            //if play service is not available
            if(ConnectionResult.SUCCESS != resultCode) {
                //If play service is supported but not installed
                if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                    //Displaying message that play service is not installed
                    GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());

                    //If play service is not supported
                    //Displaying an error message
                } else {
                }

                //If play service is available
            } else {
                //Starting intent to register device
                Intent itent = new Intent(this, GCMRegistrationIntentService.class);
                startService(itent);
            }
        }


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_calander, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        {
            int id = item.getItemId();

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){

        DatabaseHandler db = new DatabaseHandler(SetingActivity.this);
        Cursor c = db.getAllContacts();
        if (c.moveToFirst()) {
            Intent Count_down = new Intent(SetingActivity.this, Home_page.class);
            startActivity(Count_down);
            finish();
        }else{
        }
        db.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        BusProvider.getInstance().register(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
    }
    private void usePost(String email, String name, String tp,String regId){

        SharedPreferences prefs2 = getSharedPreferences("currency", MODE_WORLD_READABLE);

        String  country_defolt2 = String.valueOf(prefs2.getString("cuntry_name", " "));
        String  tp_cord_curent = String.valueOf(prefs2.getString("tp_cord_curent2", " "));
        Log.e("country_defolt2",country_defolt2);

        pDialog.show();
        User_registetion(email,country_defolt2,name,tp,regId);

    }


    //Unregistering receiver on activity paused
    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);


    }


    public void User_registetion(String email,String country_user, String name, String tp, String rg_togent){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(SERVER_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        Interface communicatorInterface = restAdapter.create(Interface.class);
        Callback<Object_register> callback = new Callback<Object_register>() {

            @Override
            public void success(Object_register models, Response response) {

                if(models.getResponseCode() == 1){

                    Intent itent = new Intent(SetingActivity.this, Splash.class);
                    finish();
                    startActivity(itent);


                }else{
                    Intent itent = new Intent(SetingActivity.this, Splash.class);
                    finish();
                    startActivity(itent);

//                    BusProvider.getInstance().post(produceErrorEvent(models.getResponseCode(), models.getMessage()));

                }

            }

            @Override
            public void failure(RetrofitError error) {
                if(error != null ){
                    Log.e(TAG, error.getMessage());
                    error.printStackTrace();
                }
//                BusProvider.getInstance().post(produceErrorEvent(-200,error.getMessage()));
            }
        };
        communicatorInterface.post_registetion(email,country_user,name,tp,rg_togent,callback);
//        country
    }


    @Produce
    public ServerEvent produceServerEvent_registetion (Object_register serverResponse) {

        return new ServerEvent(serverResponse);
    }

    @Produce
    public ErrorEvent produceErrorEvent(int errorCode, String errorMsg) {
        return new ErrorEvent(errorCode, errorMsg);
    }



    private void getBooks(){
        //While the app fetched data we are displaying a progress dialog
       String ROOT_URL ="http://projects.yogeemedia.com/preview/embassy";

        //Creating a rest adapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL)
                .build();

        //Creating an object of our api interface
        Interface api = adapter.create(Interface.class);

        //Defining the method
        api.getBooks( new Callback<Response>() {
                @Override
                public void success(Response detailsResponse, Response response2) {

                    String detailsString = getStringFromRetrofitResponse(detailsResponse);

                    try {
                        JSONObject object = new JSONObject(detailsString);

                        Log.e("List2", String.valueOf(object.getJSONArray("contacts")));

                        writeToFile(object);

                        //In here you can check if the "details" key returns a JSONArray or a String

                    } catch (JSONException e) {
                        Log.e("Listdd",e.toString());
                    }

                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e("Listd",error.toString());

                }});}



    private void get_fastaid(){
        //While the app fetched data we are displaying a progress dialog
        String ROOT_URL ="http://projects.yogeemedia.com/preview/embassy";

        //Creating a rest adapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL)
                .build();

        //Creating an object of our api interface
        Interface api = adapter.create(Interface.class);

        //Defining the method
        api.getfasttaid( new Callback<Response>() {
            @Override
            public void success(Response detailsResponse, Response response2) {

                String detailsString = getStringFromRetrofitResponse(detailsResponse);

                try {
                    JSONObject object = new JSONObject(detailsString);

                    Log.e("List", String.valueOf(object.getJSONArray("contacts")));

                    writeToFile_fest(object);

                    //In here you can check if the "details" key returns a JSONArray or a String

                } catch (JSONException e) {

                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("List",error.toString());

            }});}

        public static String getStringFromRetrofitResponse(Response response) {
            //Try to get response body
            BufferedReader reader = null;
            StringBuilder sb = new StringBuilder();
            try {

                reader = new BufferedReader(new InputStreamReader(response.getBody().in()));

                String line;

                try {
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return sb.toString();

        }



            private void writeToFile(JSONObject list) {

                ContextWrapper contextWrapper = new ContextWrapper(SetingActivity.this);
                directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);
                myInternalFile = new File(directory, "embecy");

                Log.e("contacts",list.toString());
                if (jsonmenues_stored == null) {
                    try {
                        FileOutputStream fos = new FileOutputStream(myInternalFile);
                        fos.write(list.toString().getBytes());
                        fos.close();

                        get_fastaid();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }


    private void writeToFile_fest(JSONObject list) {

        ContextWrapper contextWrapper = new ContextWrapper(SetingActivity.this);
        directory = contextWrapper.getDir(filepath, Context.MODE_PRIVATE);
        myInternalFile = new File(directory, "firstaid");

        if (jsonmenues_stored == null) {
            try {
                FileOutputStream fos = new FileOutputStream(myInternalFile);
                fos.write(list.toString().getBytes());
                fos.close();

                pDialog.cancel();
                Intent itent = new Intent(SetingActivity.this, Splash.class);
                finish();
                startActivity(itent);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void getContacts() {

        String phoneNumber = null;
        String email = null;
        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        Uri EmailCONTENT_URI =  ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
        String DATA = ContactsContract.CommonDataKinds.Email.DATA;
        StringBuffer output;
        ContentResolver contentResolver = getContentResolver();
        cursor = contentResolver.query(CONTENT_URI, null,null, null, null);

        if (cursor.getCount() > 0) {
            counter = 0;
            while (cursor.moveToNext()) {
                output = new StringBuffer();

                updateBarHandler.post(new Runnable() {
                    public void run() {
//                        pDialog.setMessage("Reading contacts : "+ counter++ +"/"+cursor.getCount());
                    }
                });
                String contact_id = cursor.getString(cursor.getColumnIndex( _ID ));
                String name = cursor.getString(cursor.getColumnIndex( DISPLAY_NAME ));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex( HAS_PHONE_NUMBER )));
                if (hasPhoneNumber > 0) {
                    output.append("\n First Name:" + name);
                    //This is to read multiple phone numbers associated with the same contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { contact_id }, null);
                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        output.append(phoneNumber);
                        Log.e("phoneNumber",phoneNumber);
                    }
                    phoneCursor.close();
                    // Read every email id associated with the contact
                    Cursor emailCursor = contentResolver.query(EmailCONTENT_URI,    null, EmailCONTACT_ID+ " = ?", new String[] { contact_id }, null);
                    while (emailCursor.moveToNext()) {
                        email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
                        output.append("\n Email:" + email);
                    }
                    emailCursor.close();
                }
                if(!phoneNumber.isEmpty()||phoneNumber.equalsIgnoreCase(" ")||phoneNumber.equalsIgnoreCase(null)){

                    Object_contect itam= new Object_contect();
                    itam.setName(name);
                    itam.setPhoneNumber(phoneNumber);
                    itam.setPrint_contect(output.toString());
                    // Add the contact to the ArrayList
                    contactList.add(itam);
                }


            }
            // ListView has to be updated using a ui thread

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter = new CustomListAdapter(SetingActivity.this, contactList);
//                    adapter = new ArrayAdapter<Object_contect>(getApplicationContext(), R.layout.list_item, R.id.text1, contactList);
//                    pDialog.cancel();
                    Contect_popup();

                }
            });

//
            // Dismiss the progressbar after 500 millisecondds
            updateBarHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                }
            }, 500);
        }

    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }

    private boolean validateUsing_libphonenumber(String countryCode, String phNumber) {

        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);
        } catch (NumberParseException e) {
            System.err.println(e);
        }

        boolean isValid = phoneNumberUtil.isValidNumber(phoneNumber);
        if (isValid) {
            String internationalFormat = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            sos_phone_final = internationalFormat;

            return true;
        } else {
            return false;
        }
    }


    private boolean validateUsing_libphonenumber2(String countryCode, String phNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);
        } catch (NumberParseException e) {
            System.err.println(e);
        }

        boolean isValid = phoneNumberUtil.isValidNumber(phoneNumber);
        if (isValid) {
            String internationalFormat = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            sos_phone2_final = internationalFormat;
            return true;
        } else {

            return false;
        }
    }

    private boolean validateUsing_libphonenumber3(String countryCode, String phNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            //phoneNumber = phoneNumberUtil.parse(phNumber, "IN");  //if you want to pass region code
            phoneNumber = phoneNumberUtil.parse(phNumber, isoCode);
        } catch (NumberParseException e) {
            System.err.println(e);
        }

        boolean isValid = phoneNumberUtil.isValidNumber(phoneNumber);
        if (isValid) {
            String internationalFormat = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            sos_phone2_final2 = internationalFormat;
            return true;
        } else {

            return false;
        }
    }



    private void Contect_popup() {
        final Dialog dialog2 = new Dialog(SetingActivity.this);
        // Include dialog.xml file
        dialog2.setContentView(R.layout.contrect_dilog);
        // Set dialog title
        dialog2.setTitle("Custom Dialog");

        ListView mListView = (ListView) dialog2.findViewById(R.id.list);
        mListView.setAdapter(adapter);
        dialog2.show();
        mListView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    //TODO Do whatever you want with the list data
                    sos_phone.setText(contactList.get(position).getPhoneNumber());
                    System.out.println("contactList.get(position)"+contactList.get(position));

                    dialog2.dismiss();
                }
            });

    }

    public class CustomListAdapter extends BaseAdapter {
        private Activity activity;
        private List<Object_contect> movieItems_pro;


        public CustomListAdapter(Activity activity, ArrayList<Object_contect> movieItems_pro) {
            this.activity = activity;
            this.movieItems_pro = movieItems_pro;
        }

        @Override
        public int getCount() {
            return this.movieItems_pro.size();
        }

        @Override
        public Object getItem(int location) {
            return movieItems_pro.get(location);

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            inflater = (LayoutInflater)SetingActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null)

                convertView = inflater.inflate(R.layout.list_item, null);


            TextView topick = (TextView) convertView.findViewById(R.id.text1);

            final Object_contect m = movieItems_pro.get(position);
            topick.setText(m.getPrint_contect());
            return convertView;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermission();
        }

    }

    private void requestPermission() {
        SetingActivity.super.requestAppPermissions(new
                        String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SEND_SMS, Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, R.string.runtime_permissions_txt
                , REQUEST_PERMISSIONS);

    }



}
