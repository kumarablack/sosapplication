package trimdevelopmentcom.sos;

/**
 * Created by Macbook on 9/17/16.
 */
import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class BroadcastService extends Service {
    public boolean isRunning = false;
    private final static String TAG = "BroadcastService";
    int millisUntilFinished;
    public static final String COUNTDOWN_BR = "trimdevelopmentcom.sos.countdown_br";
    Intent bi = new Intent(COUNTDOWN_BR);
    CountDownTimer cdt = null;

    @Override
    public void onDestroy() {

        cdt.cancel();
        Log.i(TAG, "Timer cancelled");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

             millisUntilFinished = intent.getIntExtra("data",0);
        cdt = new CountDownTimer(millisUntilFinished, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                isRunning = true;

                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));

                bi.putExtra("countdown", hms);
                bi.putExtra("stat", "0");
                sendBroadcast(bi);

            }

            @Override
            public void onFinish()
            {
                bi.putExtra("stat", "1");
                sendBroadcast(bi);
            }
        };

        cdt.start();


        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
}
