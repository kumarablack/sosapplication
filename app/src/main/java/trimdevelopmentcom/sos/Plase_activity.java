package trimdevelopmentcom.sos;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Plase_activity extends AppCompatActivity implements OnMapReadyCallback {
    String Home_url = "https://maps.googleapis.com/maps/api/directions/";
    private static final String SERVER_URL = "http://coolmanlk.com/taxi";
    ArrayList<LatLng> markerPoints;
    public GoogleMap map;
    String latitude,longitude;
    GlobalClass globalvariable;
    TextView      trip_time,disttens;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        latitude = intent.getStringExtra("lat");
        longitude = intent.getStringExtra("lon");
        String type = intent.getStringExtra("type");
        setContentView(R.layout.activity_plase_activity);
        globalvariable = (GlobalClass) Plase_activity.this.getApplicationContext();

        Log.d("fuck", String.valueOf(latitude));
        Log.d("fuck", String.valueOf(longitude));


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        TextView setting = (TextView) toolbar.findViewById(R.id.setting);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Count_down = new Intent(Plase_activity.this, SetingActivity.class);

                startActivity(Count_down);

            }
        });
        TextView toobar_title = (TextView) toolbar.findViewById(R.id.name_toll2);
        toobar_title.setText("NEAR BY "+type);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map2);
        mapFragment.getMapAsync(this);
        trip_time =(TextView)findViewById(R.id.trip_time) ;
                disttens =(TextView)findViewById(R.id.disttens) ;


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_calander, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;
        map.getUiSettings().setZoomControlsEnabled(true);
        LatLng  markerLatLng_stat = new LatLng(Double.valueOf(latitude),Double.valueOf(longitude));
        LatLng  markerLatLng1_finish = new LatLng(Double.valueOf(globalvariable.getLatitude()),Double.valueOf(globalvariable.getLongitude()));
        add_dropoff(markerLatLng_stat, markerLatLng1_finish);

    }


    public void add_dropoff(LatLng startLatLng, LatLng endLatLng) {
        markerPoints = new ArrayList<>();

        markerPoints.add(startLatLng);
        markerPoints.add(endLatLng);

        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();
        // Setting the position of the marker
        options.position(startLatLng).title("Pick Up").snippet("Pick");

        MarkerOptions options2 = new MarkerOptions();
        options2.position(endLatLng).title("Drop Off").snippet("Drop");

        /**
         * For the start location, the color of marker is GREEN and
         * for the end location, the color of marker is RED.
         */
        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.placeholder_point));
        } else if (markerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.placeholder_point));
            options2.icon(BitmapDescriptorFactory.fromResource(R.drawable.placeholder_point));
        }

        // Add new marker to the Google Map Android API V2
        map.addMarker(options).showInfoWindow();
        map.addMarker(options2).showInfoWindow();

        // Checks, whether start and end locations are captured
        if (markerPoints.size() >= 2) {

            LatLng origin = markerPoints.get(0);
            LatLng dest = markerPoints.get(1);

            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(origin, dest);

            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }
    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl_Two(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask_Two parserTask = new ParserTask_Two();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask_Two extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance_km = null,trip_time2 = null;


            LatLng position = null;
            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);


                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    distance_km = point.get("distance_km");
                    trip_time2 = point.get("time");
                    position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.RED);
                System.out.println("trip_time" + trip_time2+" "+" distance_km" + distance_km);
//
                trip_time.setText("Time :" + trip_time2);
                disttens.setText("Distance :" + distance_km);
            }

            map.addPolyline(lineOptions);//draw the route

            map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
            map.setBuildingsEnabled(true);
            map.animateCamera(CameraUpdateFactory.zoomTo(15), 1500, null);
            final String Distance_km = distance_km;

            map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {

                    View v = ((LayoutInflater) Plase_activity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_moreinfo, null);

                    v.setLayoutParams(new RelativeLayout.LayoutParams(100, RelativeLayout.LayoutParams.WRAP_CONTENT));

                    TextView place_count = (TextView) v.findViewById(R.id.textView8);
                    place_count.setText(Distance_km);


                    return v;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    return null;
                }
            });
            // map.addMarker(markerOptions);

        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl_Two(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {

        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Sensor enabled
        String sensor = "sensor=false";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = Home_url + output + "?" + parameters;

        return url;
    }

}
