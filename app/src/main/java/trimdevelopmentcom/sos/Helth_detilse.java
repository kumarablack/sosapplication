package trimdevelopmentcom.sos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



public class Helth_detilse extends AppCompatActivity implements View.OnClickListener  {

    ListView listView2;
    private LayoutInflater inflater;
    TextView map,embce,sos,notifi,add;
    private String[] Ride_stetest = {"Planed", "Previous", "current", "Previous"};
    String firstaid_details_name;
    CustomListAdapter_helth apter_Rides;
    JSONArray  firstaid_point_not;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        String userProfileString = intent.getStringExtra("message");
//        String userProfileString=getArguments().getString("message");
        try {
            JSONArray jsonObject=new JSONArray(userProfileString);
            for(int i=0;i<jsonObject.length();i++){
                JSONObject menuObject = jsonObject.getJSONObject(i);
                firstaid_details_name =menuObject.getString("firstaid_details_name");
                firstaid_point_not =menuObject.getJSONArray("firstaid_point_not");


            }

            for(int i=0;i<firstaid_point_not.length();i++){
                JSONObject m = firstaid_point_not.getJSONObject(i);
                String firstaid_details_not =m.getString("firstaid_details_not"+i);
                Obj_Helth itam = new Obj_Helth();
                itam.setNote(firstaid_details_not);
                Data_coltrola.helth_note.add(itam);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_helth_detilse);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        TextView setting = (TextView) toolbar.findViewById(R.id.setting);
        setSupportActionBar(toolbar);

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Count_down = new Intent(Helth_detilse.this, SetingActivity.class);
                startActivity(Count_down);
            }
        });
        TextView toobar_title = (TextView) toolbar.findViewById(R.id.name_toll2);
        toobar_title.setText("Fasted Training");
        TextView back =(TextView)toolbar.findViewById(R.id.info);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        listView2=(ListView)findViewById(R.id.listView2);
        TextView topic = (TextView)findViewById(R.id.topic);
        topic.setText(firstaid_details_name);







        apter_Rides  = new CustomListAdapter_helth(Helth_detilse.this, Data_coltrola.helth_note);
        listView2.setAdapter(apter_Rides);



    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {



        }

    }

    public class CustomListAdapter_helth extends BaseAdapter {
        private Activity activity;
        private List<Obj_Helth> movieItems_pro;


        public CustomListAdapter_helth(Activity activity, ArrayList<Obj_Helth> movieItems_pro) {
            this.activity = activity;
            this.movieItems_pro = movieItems_pro;
        }

        @Override
        public int getCount() {
            return this.movieItems_pro.size();
        }

        @Override
        public Object getItem(int location) {
            return movieItems_pro.get(location);

        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            inflater = (LayoutInflater)Helth_detilse.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null)
                convertView = inflater.inflate(R.layout.helth_sell_data, null);
            TableRow tach = (TableRow) convertView.findViewById(R.id.tableRow2);
            TextView infromation = (TextView) convertView.findViewById(R.id.infromation);




            final Obj_Helth m = movieItems_pro.get(position);

            infromation.setText(m.getNote());
            return convertView;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_calander, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

}

